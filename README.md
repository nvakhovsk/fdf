# ** FDF ** #
## This is a project where you have to display a 2d map file: It will display it with an isometric and parallel view. 
## Used with mlx on X11 version. 
## **Usage:** 
* $> make
* $> usage: ./fdf maps/map_to_test.fdf
## **Controls:**
* Basic controls: 'z' , 'x' , to up Z-Values. '+' , '-' , to use Zoom. 
* Close the program	 esc 
## You also need the "minilibx" to be installed on your device. 
![Screen Shot 2017-04-29 at 9.24.24 AM.png](https://bitbucket.org/repo/akbbM88/images/414085711-Screen%20Shot%202017-04-29%20at%209.24.24%20AM.png)
![Screen Shot 2017-04-29 at 9.26.54 AM.png](https://bitbucket.org/repo/akbbM88/images/442929306-Screen%20Shot%202017-04-29%20at%209.26.54%20AM.png)
![Screen Shot 2017-04-29 at 9.28.22 AM.png](https://bitbucket.org/repo/akbbM88/images/3729595082-Screen%20Shot%202017-04-29%20at%209.28.22%20AM.png)
![Screen Shot 2017-04-29 at 9.29.46 AM.png](https://bitbucket.org/repo/akbbM88/images/3850223497-Screen%20Shot%202017-04-29%20at%209.29.46%20AM.png)