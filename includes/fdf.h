/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/02 12:21:29 by nmatushe          #+#    #+#             */
/*   Updated: 2017/03/09 12:56:04 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <mlx.h>
# include <math.h>
# include <fcntl.h>
# include <stdio.h>
# include "libft.h"
# define WW 1920
# define WH 1080

# define ESC 53
# define Z 6
# define X 7
# define I 34
# define P 35
# define PL 69
# define MN 78
# define N1 83
# define N2 84
# define UP 126
# define DWN 125
# define LFT 123
# define RGT 124

typedef struct	s_cmap
{
	int			cl;
	float		x;
	float		y;
	float		z;
}				t_cmap;

typedef struct	s_dim
{
	int			h;
	int			w;
	float		zm;
	float		step;
	int			al;
	int			p;
}				t_dim;

typedef struct	s_graph
{
	void		*init;
	void		*win;
}				t_graph;

typedef struct	s_all
{
	t_cmap		***r;
	t_cmap		***m;
	t_dim		*dim;
	t_graph		*mlx;
	int			flag;
}				t_all;

typedef struct	s_brezen
{
	float		dx;
	float		dy;
	float		sx;
	float		sy;
	float		err0;
	float		err1;
}				t_brezen;

t_cmap			***ft_parallel(t_cmap ***m, t_dim *dim);
t_cmap			***ft_isometric(t_cmap ***m, t_dim *dim);

void			ft_z(int keycode, t_all *p);
void			ft_disp(t_all *p, int flag);
void			ft_proj(int keycode, t_all *p);
void			ft_zoom(int keycode, t_all *p);
void			ft_rot(int keycode, t_all *p);

t_cmap			***ft_map_split(char **map, t_dim **dim);
t_cmap			***ft_change_z(t_cmap ***m, t_dim *d, int flag);
t_cmap			***ft_rotate(t_cmap ***m, t_dim *dim);

int				ft_atoi_base(char *str, int base);
int				ft_err(void);

void			ft_menu(t_graph *p);
void			ft_algo(t_cmap ***crd_map, t_dim *dim, char *name);
void			ft_draw(t_cmap ***r, t_dim *dim, t_graph *mlx);

#endif
