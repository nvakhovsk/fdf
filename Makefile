NAME = fdf

HEAD = includes/

VPATH = srcs:includes

FLAGS = -Wall -Wextra -Werror -I $(HEAD)

MLX = -lmlx -framework AppKit -framework OpenGl

SRCS = main.c									\
		ft_atoi_base.c							\
		algo.c									\
		getmap.c								\
		hooks.c 								\
		proj.c 									\

BINS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(BINS)
		make re -C libft/ fclean && make -C libft/
		gcc -o $(NAME) -I libft/includes $(BINS) $(FLAGS) -L. libft/libft.a $(MLX)

%.o: %.c
	gcc -I libft/includes $(FLAGS) -c -o $@ $<

clean:
	/bin/rm -f $(BINS)

fclean: clean
	/bin/rm -f $(NAME)

re: fclean all
