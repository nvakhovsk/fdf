/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstiter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 14:23:04 by nmatushe          #+#    #+#             */
/*   Updated: 2016/12/01 16:19:01 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			ft_lstiter(t_list *lst, void (*f)(t_list *elem))
{
	t_list	*tlst;
	t_list	*cur;

	tlst = lst;
	while (tlst)
	{
		cur = tlst->next;
		(*f)(tlst);
		tlst = cur;
	}
}
