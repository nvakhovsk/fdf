/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/07 18:24:53 by nmatushe          #+#    #+#             */
/*   Updated: 2017/03/08 11:52:13 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_z(int key, t_all *all)
{
	if (key == 6)
		all->m = ft_change_z(all->m, all->dim, 1);
	else
		all->m = ft_change_z(all->m, all->dim, 0);
	all->r = (all->flag) ? ft_isometric(all->m, all->dim)
		: ft_parallel(all->m, all->dim);
	ft_draw(all->r, all->dim, all->mlx);
}

void	ft_disp(t_all *all, int flag)
{
	int i;
	int j;

	i = -1;
	while (++i < all->dim->h)
	{
		j = -1;
		while (++j < all->dim->w)
		{
			if (flag == 1)
				all->r[i][j]->y -= 7;
			if (flag == 2)
				all->r[i][j]->y += 7;
			if (flag == 3)
				all->r[i][j]->x += 7;
			if (flag == 4)
				all->r[i][j]->x -= 7;
		}
	}
	ft_draw(all->r, all->dim, all->mlx);
}

void	ft_proj(int key, t_all *all)
{
	if (key == I)
	{
		all->r = ft_isometric(all->m, all->dim);
		all->flag = 1;
	}
	else
	{
		all->r = ft_parallel(all->m, all->dim);
		all->flag = 0;
	}
	ft_draw(all->r, all->dim, all->mlx);
}

void	ft_zoom(int key, t_all *all)
{
	if (key == PL && all->dim->zm <= 38)
		all->dim->zm += all->dim->step;
	if (key == MN)
		all->dim->zm -= all->dim->step;
	all->r = (all->flag) ? ft_isometric(all->m, all->dim)
		: ft_parallel(all->m, all->dim);
	ft_draw(all->r, all->dim, all->mlx);
}

void	ft_rot(int key, t_all *all)
{
	all->dim->al += (key == N1) ? 9 : 0;
	all->dim->p += (key == N2) ? 9 : 0;
	all->r = (all->flag) ? ft_isometric(all->m, all->dim)
		: ft_parallel(all->m, all->dim);
	ft_draw(all->r, all->dim, all->mlx);
}
