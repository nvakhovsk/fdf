/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 15:08:04 by nmatushe          #+#    #+#             */
/*   Updated: 2017/03/08 12:04:51 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void			ft_menu(t_graph *p)
{
	mlx_string_put(p->init, p->win, 700, 460, 16777215, "MENU:");
	mlx_string_put(p->init, p->win, 700, 500, 16777215,
		"Press i  for ISOMETRIC projection");
	mlx_string_put(p->init, p->win, 700, 520, 16777215,
		"Press p for PARALLEL projection");
}

static int		ft_lines(int fd, int *j)
{
	int		i;
	char	*temp;
	int		flag;

	i = 0;
	while ((flag = get_next_line(fd, &temp)))
	{
		if (flag > 0)
		{
			*j = (i == 0) ? ft_strlen(temp) : *j;
			free(temp);
			i++;
		}
		else if (flag < 0)
			return (-1);
	}
	close(fd);
	return (i);
}

static int		ft_get_map(int fd, char ***map, char *name)
{
	char	*temp;
	int		flag;
	int		i;
	int		j;

	j = 0;
	if ((i = ft_lines(fd, &j)) == -1 ||
		!(*map = (char **)malloc(sizeof(char *) * (i + 1))))
		return (0);
	fd = open(name, O_RDONLY);
	i = -1;
	while ((flag = get_next_line(fd, &temp)))
	{
		if (flag > 0)
		{
			(*map)[++i] = (char *)malloc(sizeof(char *) * (j + 1));
			(*map)[i] = ft_strdup(temp);
			free(temp);
		}
		else if (flag < 0)
			return (0);
	}
	(*map)[++i] = NULL;
	close(fd);
	return (1);
}

static t_dim	*ft_dim(t_dim *dim)
{
	dim->zm = WW / 6 / dim->w;
	if ((int)dim->zm == 0)
		dim->zm = 0.5;
	dim->step = dim->zm / 6;
	return (dim);
}

int				main(int argc, char **argv)
{
	int			fd;
	char		**map;
	t_cmap		***c_map;
	t_dim		*dim;

	if (argc == 2)
	{
		if ((fd = open(argv[1], O_RDONLY)) < 0)
			return (ft_err());
		if (fd < 0 || !ft_get_map(fd, &map, argv[1]))
			return (ft_err());
		dim = (t_dim *)malloc(sizeof(t_dim));
		if (!(c_map = ft_map_split(map, &dim)))
			return (ft_err());
		dim = ft_dim(dim);
		free(map);
		ft_algo(c_map, dim, argv[1]);
		return (0);
	}
	return (ft_err());
}
