/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getmap.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/07 19:11:42 by nmatushe          #+#    #+#             */
/*   Updated: 2017/03/09 13:00:44 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int				ft_err(void)
{
	ft_putendl("error :)");
	return (0);
}

static int		ft_str2len(char **str)
{
	int	i;

	i = -1;
	while (str[++i])
		;
	return (i);
}

static int		ft_splitcrd(char *str, int fl)
{
	int		i;
	int		res;
	char	**arr;

	i = -1;
	arr = ft_strsplit(str, ',');
	if (fl == 0)
		res = ft_atoi(ft_strdup(arr[0]));
	else if (!arr[1])
		res = ft_atoi_base("FFFFFF", 16);
	else if (arr[1])
		res = ft_atoi_base(ft_strsub(arr[1], 2, ft_strlen(arr[1]) - 2), 16);
	else
		return (-1);
	free(arr);
	return (res);
}

static int		ft_newdot(float x, float y, char *str, t_cmap **list)
{
	int			color;

	*list = (t_cmap *)malloc(sizeof(t_cmap));
	(*list)->x = x;
	(*list)->y = y;
	(*list)->z = ft_splitcrd(str, 0);
	if ((color = ft_splitcrd(str, 1)) < 0)
		return (0);
	else
		(*list)->cl = color;
	return (1);
}

t_cmap			***ft_map_split(char **map, t_dim **dim)
{
	float		i;
	float		j;
	t_cmap		***res;
	char		**temp;

	(*dim)->h = ft_str2len(map);
	res = (t_cmap ***)malloc(sizeof(t_cmap **) * (*dim)->h);
	i = -1;
	while (map[(int)++i])
	{
		temp = ft_strsplit(map[(int)i], ' ');
		(*dim)->w = (i == 0) ? ft_str2len(temp) : (*dim)->w;
		if (ft_str2len(temp) != (*dim)->w)
			return (NULL);
		res[(int)i] = (t_cmap **)malloc(sizeof(t_cmap *) * (*dim)->w);
		j = -1;
		while ((int)++j < ft_str2len(temp))
		{
			if (ft_newdot(j, i, temp[(int)j], &res[(int)i][(int)j]) < 0)
				return (NULL);
		}
		free(temp);
	}
	return (res);
}
