/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   proj.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/07 19:02:28 by nmatushe          #+#    #+#             */
/*   Updated: 2017/03/08 11:52:25 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static float	ft_mmz(t_cmap ***m, t_dim *d)
{
	int		r;
	int		c;
	float	max;
	float	min;
	float	mmz;

	r = 0;
	max = m[0][0]->z;
	min = m[0][0]->z;
	while (++r < d->h)
	{
		c = 0;
		while (++c < d->w)
		{
			max = (m[r][c]->z > max) ? m[r][c]->z : max;
			min = (m[r][c]->z < min) ? m[r][c]->z : min;
		}
	}
	mmz = (max - min) * 0.1;
	return (mmz);
}

t_cmap			***ft_change_z(t_cmap ***m, t_dim *d, int flag)
{
	int		r;
	int		c;
	float	mmz;

	mmz = ft_mmz(m, d);
	r = -1;
	while (++r < d->h)
	{
		c = -1;
		while (++c < d->w)
		{
			if (m[r][c]->z != 0)
			{
				if (flag == 0)
					if ((m[r][c]->z = m[r][c]->z - mmz) == 0)
						m[r][c]->z = m[r][c]->z - mmz;
				if (flag == 1)
					if ((m[r][c]->z = m[r][c]->z + mmz) == 0)
						m[r][c]->z = m[r][c]->z + mmz;
			}
		}
	}
	return (m);
}

t_cmap			***ft_isometric(t_cmap ***m, t_dim *dim)
{
	int			r;
	int			c;
	t_cmap		***a;

	r = -1;
	a = (t_cmap ***)malloc(sizeof(t_cmap **) * dim->h);
	while (++r < dim->h)
	{
		c = -1;
		a[r] = (t_cmap **)malloc(sizeof(t_cmap *) * dim->w);
		while (++c < dim->w)
		{
			a[r][c] = (t_cmap *)malloc(sizeof(t_cmap));
			a[r][c]->x = (m[r][c]->y - m[r][c]->x) * sin(dim->al * M_PI / 180);
			a[r][c]->x = (a[r][c]->x - dim->w / 2) * dim->zm + WW / 2;
			a[r][c]->y = ((m[r][c]->x + m[r][c]->y) * cos(dim->p * M_PI / 180)
				- m[r][c]->z);
			a[r][c]->y = (a[r][c]->y - dim->h / 2) * dim->zm + WH / 2;
			a[r][c]->cl = m[r][c]->cl;
		}
	}
	return (a);
}

t_cmap			***ft_parallel(t_cmap ***m, t_dim *dim)
{
	int			r;
	int			c;
	t_cmap		***a;

	r = -1;
	a = (t_cmap ***)malloc(sizeof(t_cmap **) * dim->h);
	while (++r < dim->h)
	{
		c = -1;
		a[r] = (t_cmap **)malloc(sizeof(t_cmap *) * dim->w);
		while (++c < dim->w)
		{
			a[r][c] = (t_cmap *)malloc(sizeof(t_cmap));
			a[r][c]->x = m[r][c]->x;
			a[r][c]->x = (a[r][c]->x - dim->w / 2) * dim->zm + WW / 2;
			a[r][c]->y = m[r][c]->y * cos(dim->al * M_PI / 180)
				+ m[r][c]->z * sin(dim->al * M_PI / 180);
			a[r][c]->y = (a[r][c]->y - dim->h / 2) * dim->zm + WH / 2;
			a[r][c]->cl = m[r][c]->cl;
		}
	}
	return (a);
}
