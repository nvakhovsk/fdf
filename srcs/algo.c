/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/04 13:11:49 by nmatushe          #+#    #+#             */
/*   Updated: 2017/03/08 12:03:47 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		ft_brezenhaim(t_cmap d1, t_cmap d2, t_graph *mlx)
{
	t_brezen	*brzn;

	brzn = (t_brezen *)malloc(sizeof(t_brezen));
	brzn->dx = abs((int)d2.x - (int)d1.x);
	brzn->dy = abs((int)d2.y - (int)d1.y);
	brzn->sx = (d1.x < d2.x) ? 1 : -1;
	brzn->sy = (d1.y < d2.y) ? 1 : -1;
	brzn->err0 = brzn->dx - brzn->dy;
	while ((int)d1.x != (int)d2.x || (int)d1.y != (int)d2.y)
	{
		brzn->err1 = brzn->err0 * 2;
		if (brzn->err1 > -brzn->dy)
		{
			brzn->err0 -= brzn->dy;
			d1.x += brzn->sx;
		}
		if (brzn->err1 < brzn->dx)
		{
			brzn->err0 += brzn->dx;
			d1.y += brzn->sy;
		}
		mlx_pixel_put(mlx->init, mlx->win, d1.x, d1.y, d1.cl);
	}
	free(brzn);
}

void		ft_net(t_cmap ***m, t_dim *dim, t_graph *mlx)
{
	int i;
	int j;

	i = -1;
	while (++i < (dim->h))
	{
		j = -1;
		while (++j < (dim->w - 1))
			if ((m)[i][j]->x > 0 && (m)[i][j]->y > 0 && (m)[i][j + 1]->x > 0
				&& (m)[i][j + 1]->y > 0)
				ft_brezenhaim(*m[i][j], *m[i][j + 1], mlx);
	}
	i = -1;
	{
		j = -1;
		while (++j < (dim->w))
		{
			i = -1;
			while (++i < (dim->h - 1))
				if ((m)[i][j]->x > 0 && (m)[i][j]->y > 0 && (m)[i + 1][j]->x > 0
					&& (m)[i + 1][j]->y > 0)
					ft_brezenhaim(*m[i][j], *m[i + 1][j], mlx);
		}
	}
}

void		ft_draw(t_cmap ***r, t_dim *dim, t_graph *mlx)
{
	int			i;
	int			j;

	i = -1;
	mlx_clear_window(mlx->init, mlx->win);
	while (++i < dim->h)
	{
		j = -1;
		while (++j < dim->w)
		{
			if (r[i][j]->x > 0 && r[i][j]->y > 0)
				mlx_pixel_put(mlx->init, mlx->win, r[i][j]->x, r[i][j]->y,
					r[i][j]->cl);
		}
	}
	ft_net(r, dim, mlx);
}

int			ft_hooks(int key, t_all *all)
{
	if (key == ESC)
		exit(0);
	if ((key == Z || key == X) && all->flag != 5)
		ft_z(key, all);
	if (key == UP)
		ft_disp(all, 1);
	if (key == DWN)
		ft_disp(all, 2);
	if (key == RGT)
		ft_disp(all, 3);
	if (key == LFT)
		ft_disp(all, 4);
	if (key == I || key == P)
		ft_proj(key, all);
	if ((key == PL || key == MN) && all->flag != 5)
		ft_zoom(key, all);
	if ((key == N1 || key == N2) && all->flag != 5)
		ft_rot(key, all);
	return (0);
}

void		ft_algo(t_cmap ***cmap, t_dim *dim, char *n)
{
	t_graph		*mlx;
	t_cmap		***res;
	t_all		*all;

	mlx = (t_graph *)malloc(sizeof(t_graph));
	mlx->init = mlx_init();
	mlx->win = mlx_new_window(mlx->init, WW, WH, n);
	all = (t_all *)malloc(sizeof(t_all));
	all->dim = dim;
	all->mlx = mlx;
	all->m = cmap;
	all->flag = 5;
	all->dim->al = 315;
	all->dim->p = 315;
	ft_menu(mlx);
	res = ft_isometric(cmap, dim);
	all->r = res;
	mlx_key_hook(mlx->win, ft_hooks, all);
	mlx_loop(mlx->init);
}
